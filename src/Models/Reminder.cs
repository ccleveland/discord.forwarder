﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace discord.forwarder.Models
{
    public partial class Reminder
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long Author { get; set; }
        public DateTime Created { get; set; }
        public DateTime Scheduled { get; set; }
        public string Message { get; set; }

        public Reminder(long author, DateTime created, DateTime scheduled, string message)
        {
            Author = author;
            Created = created;
            Scheduled = scheduled;
            Message = message;
        }
    }
}
