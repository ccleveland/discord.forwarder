﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using Microsoft.Extensions.DependencyInjection;
using discord.forwarder.Services;

namespace discord.forwarder
{
    class Config
    {
        public string ApplicationName { get; set; }
        public int Version { get; set; }
        public string AuthToken { get; set; }
        public string CommandPrefix { get; set; }
        public UInt64 GuildId { get; set; }
        public UInt64 DefaultChannel { get; set; }
        public string AdminRole { get; set; }
        public string ImapUrl { get; set; }
        public string EmailUser { get; set; }
        public string EmailPass { get; set; }
        public string[] EmailFrom { get; set; }
        public bool Filter { get; set; }
        public string ConnString { get; set; }
        public bool Debug { get; set; }
    }
    class Program
    {
        public IConfigurationRoot Configuration { get; set; }
        private CommandService commands;
        private IServiceProvider services;
        private DiscordSocketClient client;
        private Config config;
        static void Main(string[] args)
        {
            Program run = new Program();
            run.Startup();
        }
        public void Startup()
        {
            Log(new LogMessage(LogSeverity.Info, "Program", "Bot starting up running .Net Core " + Environment.Version.ToString(), null));
            config = new Config();
            var builder = new ConfigurationBuilder();
            var appsettings = JsonConfigurationExtensions.AddJsonFile(builder, "./config/appsettings.json");
            Configuration = builder.Build();
            config.AuthToken = Configuration.GetSection("Configuration:Discord:AuthToken").Value;
            config.CommandPrefix = Configuration.GetSection("Configuration:Discord:CommandPrefix").Value;
            config.GuildId = Convert.ToUInt64(Configuration.GetSection("Configuration:Discord:GuildId").Value);
            config.DefaultChannel = Convert.ToUInt64(Configuration.GetSection("Configuration:Discord:DefaultChannel").Value);
            config.AdminRole = Configuration.GetSection("Configuration:Discord:AdminRole").Value;
            config.ImapUrl = Configuration.GetSection("Configuration:Email:ImapUrl").Value;
            config.EmailUser = Configuration.GetSection("Configuration:Email:User").Value;
            config.EmailPass = Configuration.GetSection("Configuration:Email:Password").Value;
            config.Filter = Convert.ToBoolean(Configuration.GetSection("Configuration:Email:Filter").Value);
            config.EmailFrom = Configuration.GetSection("Configuration:Email:From").GetChildren().Select(x => x.Value).ToArray();
            config.ConnString = Configuration.GetSection("Configuration:Reminder:ConnectionString").Value;
            config.Debug = Convert.ToBoolean(Configuration.GetSection("Configuration:Debug").Value);
            ManageDiscord();
        }
        public IServiceProvider ConfigureServices(IServiceCollection services)
            => services.AddEntityFrameworkNpgsql()
            .AddDbContext<ReminderContext>()
            .AddTransient<ReminderService>(s => new ReminderService(config.ConnString, config.Debug))
            .BuildServiceProvider();
        public void ManageDiscord()
            => this.MainAsync().GetAwaiter().GetResult();
        public async Task MainAsync()
        {
            client = new DiscordSocketClient();
            Email email = new Email(config.ImapUrl, config.EmailUser, config.EmailPass);
            ReminderService reminder = new ReminderService(config.ConnString, config.Debug);
            commands = new CommandService();
            services = ConfigureServices(new ServiceCollection());
            await InstallCommands();
            await client.LoginAsync(TokenType.Bot, config.AuthToken);
            await client.SetGameAsync("clevelandcoding.com | " + config.CommandPrefix + "help");
            await client.StartAsync();
            client.Ready += () =>
        {
            Console.WriteLine("Bot is connected!");
            email.NotifyEmail(client, config.GuildId, config.DefaultChannel, config.Filter, config.EmailFrom);
            reminder.Remind(client);
            return Task.CompletedTask;
        };
            await Task.Delay(-1);
        }
        public async Task InstallCommands()
        {
            client.Log += Log;
            client.MessageReceived += HandleCommand;

            await commands.AddModulesAsync(Assembly.GetEntryAssembly(), services);
        }
        private async Task HandleCommand(SocketMessage message)
        {
            var SocketMessage = message as SocketUserMessage;
            if (message == null) return;
            int argPos = 0;
            if (!(SocketMessage.HasCharPrefix(config.CommandPrefix.ToCharArray()[0], ref argPos) || SocketMessage.HasMentionPrefix(client.CurrentUser, ref argPos))) return;
            var context = new CommandContext(client, SocketMessage);
            var result = await commands.ExecuteAsync(context, argPos, services);
            if (!result.IsSuccess)
            {
                await context.Channel.SendMessageAsync(result.ErrorReason);
            }
        }
        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}