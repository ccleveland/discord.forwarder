using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using discord.forwarder.Services;
using discord.forwarder.Models;

namespace discord.forwarder
{
    public class CommandContextCommands : ModuleBase
    {
        public CommandService commands { get; }
        private ReminderService reminderService { get; }

        CommandContextCommands(CommandService command, ReminderService reminder)
        {
            commands = command;
            reminderService = reminder;
        }
        [Command("say"), Summary("Echo a message.")]
        public async Task Say([Remainder, Summary("The text to echo")] string echo)
        {
            await ReplyAsync(echo);
        }
        [Command("saytts"), Summary("Echo a message w/ tts.")]
        public async Task SayTTS([Remainder, Summary("The text ot echo")]string echo)
        {
            await ReplyAsync(echo, true, null, null);
        }
        [Command("help"), Summary("Returns a list of commands.")]
        public async Task Help()
        {
            var CommandList = commands.Commands;
            EmbedBuilder embedBuilder = new EmbedBuilder();
            foreach (CommandInfo c in CommandList)
            {
                string embedFieldText = c.Summary ?? "No Description";
                embedBuilder.AddField(c.Name.ToString(), c.Summary.ToString());
            }
            await ReplyAsync("Available commands and descriptions: ", false, embedBuilder.Build(), null);
        }
        [Command("botinfo"), Summary("Returns info about the bot")]
        public async Task BotInfo()
        {
            EmbedBuilder builder = new EmbedBuilder();
            builder.AddField("Available on Docker Hub", "https://hub.docker.com/r/ccleveland/discord.forwarder/");
            builder.AddField("Source Code Available here", "https://git.clevelandcoding.com/ccleveland/discord.forwarder");
            builder.AddField("Submit an issue", "https://help.clevelandcoding.com");
            await ReplyAsync("Bot development, hosting, and issue reporting info: ", false, builder.Build(), null);
        }
        [Command("square"), Summary("Squares a number.")]
        public async Task Square([Summary("The number to square.")]int num)
        {
            await Context.Channel.SendMessageAsync($"{num}^2 = {Math.Pow(num, 2)}");
        }
        [Command("userinfo"), Summary("Returns the info about a user. Either the caller or specified user.")]
        [Alias("user", "whois")]
        public async Task UserInfo([Summary("The (optional) user to get the info of")] IUser user = null)
        {
            var userInfo = user ?? Context.Client.CurrentUser;
            await ReplyAsync($"{userInfo.Username}#{userInfo.Discriminator}");
        }
        [Command("undo"), Summary("Removes the output of the previous command")]
        public async Task Undo()
        {
            IEnumerable<IMessage> messages = await Context.Channel.GetMessagesAsync(25).FlattenAsync();
            List<IMessage> toDelete = new List<IMessage>();
            foreach (var m in messages)
            {
                if ((m.Author.ToString() == Context.Client.CurrentUser.ToString() && toDelete.Count < 2) || m.Content == "!undo")
                {
                    toDelete.Add(m);
                }
            }
            await ((ITextChannel)Context.Channel).DeleteMessagesAsync(toDelete);
        }
        [Command("bing"), Summary("Bong.")]
        public async Task Bong()
        {
            await ReplyAsync("Bong!", true, null, null);
            await ReplyAsync("https://youtu.be/oguvSPdtHQ8?t=11");
        }
        [Command("--force--all"), Summary("--force --all commands")]
        public async Task ForceAll()
        {
            await Say("Running with --force --all");
            await SayTTS("COMMANDS --FORCE --ALL");
            await Help();
            await BotInfo();
            await Square(42);
            await UserInfo();
            await Bong();
        }
        [Command("reminder", RunMode = RunMode.Async), Summary("Set a reminder ex. !reminder 4h pull")]
        public async Task RemindMe(string duration, [Remainder]string message)
        {
            Regex re = new Regex("(\\d+)([DdHhMmSs])");
            MatchCollection matches = re.Matches(duration);
            DateTime now = System.DateTime.Now;
            int seconds = 0;
            foreach (Match m in matches)
            {
                var g = m.Groups[0].Value;
                var label = g.Substring(g.Length - 1);
                double dur = Convert.ToDouble(g.Remove(g.Length - 1));
                switch (label.ToUpper())
                {
                    case "D":
                        seconds += Convert.ToInt32(dur * 24 * 60 * 60);
                        break;
                    case "H":
                        seconds += Convert.ToInt32(dur * 60 * 60);
                        break;
                    case "M":
                        seconds += Convert.ToInt32(dur * 60);
                        break;
                    case "S":
                        seconds += Convert.ToInt32(dur);
                        break;
                }
            }
            await Context.Channel.SendMessageAsync("Your reminder has been set for " +
                 DateTime.Now.AddSeconds(seconds).ToString());
            reminderService.AddReminder(
                 new Reminder((long)Context.User.Id, DateTime.Now.ToUniversalTime(), DateTime.Now.AddSeconds(seconds).ToUniversalTime(), message));
        }
        [Command ("lsreminder", RunMode = RunMode.Async), Summary("Show a list of your reminders !show reminders")]
        public async Task ShowReminders()
        {
            await reminderService.ListReminders(Context.Client, Context.User);
        }

        [Command ("delreminder", RunMode = RunMode.Async), Summary("Remove a reminder from your stored reminders !remove reminder 1")]
        public async Task RemoveReminder(string indexstr)
        {
            int index = int.Parse(indexstr);
            await reminderService.RemoveReminder(Context.User, index);
        }
    }
}