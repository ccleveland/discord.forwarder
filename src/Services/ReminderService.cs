﻿using Microsoft.EntityFrameworkCore;
using Discord;
using Discord.WebSocket;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Collections.Generic;
using discord.forwarder.Models;
using System.Text;

namespace discord.forwarder.Services
{
    public class ReminderService
    {
        private string ConnStr;
        private bool Debug;
        public ReminderService(string connStr, bool debug)
        {
            ConnStr = connStr;
            Debug = debug;
        }

        public async void Remind(DiscordSocketClient client)
        {
            while (true)
            {
                SendReminder(client);
                await Task.Delay(TimeSpan.FromSeconds(30));
            }
        }

        private async void SendReminder(DiscordSocketClient client)
        {
            using (var context = new ReminderContext(ConnStr, Debug))
            {
                var rems = await context.Reminders.AsNoTracking().Where(r => (r.Scheduled <= DateTime.Now)).ToListAsync();
                foreach (Reminder r in rems)
                {
                    var eb = new EmbedBuilder();
                    eb.AddField("Your reminder is here: ", r.Message);
                    await client.GetUser((ulong)r.Author).SendMessageAsync(null, false, eb.Build(), null);
                }
                context.RemoveRange(rems);
                await context.SaveChangesAsync();
            }
        }
        public async void AddReminder(Reminder r)
        {
            using (var context = new ReminderContext(ConnStr, Debug))
            {
                await context.AddAsync(r);
                await context.SaveChangesAsync();
            }
        }
        public async Task RemoveReminder(IUser user, int index)
        {
            using (var context = new ReminderContext(ConnStr, Debug))
            {
                    long luser = (long)user.Id;
                    var stored = await context.Reminders.Where(r => r.Author == luser).OrderBy(r => r.Scheduled).ToArrayAsync();
                    if(stored.Length > 0)
                    {
                        context.Reminders.Remove(stored[index]);
                        await context.SaveChangesAsync();
                    }
            }
        }
        public async Task ListReminders(IDiscordClient client, IUser user)
        {
            DiscordSocketClient socketClient = (DiscordSocketClient)client;
            long luser = (long)user.Id;
            try
            {
                using (var context = new ReminderContext(ConnStr, Debug))
                {

                    List<Reminder> rems = await context.Reminders.Where(r => r.Author == luser).OrderBy(r => r.Scheduled).ToListAsync();
                    var sb = new StringBuilder();
                    sb.Append(String.Format("| {0,4} | {1,20} | {2,128} |\n", "Index", "Scheduled", "Message"));
                    var eb = new EmbedBuilder();
                    for (int i = 0; i < rems.Count; i++)
                    {
                        sb.Append(String.Format("| {0,4:D4} | {1, 20} | {2,128} |\n", i, rems[i].Scheduled.ToLocalTime().ToString(), rems[i].Message));
                    }
                    eb.AddField("Reminders", sb.ToString());


                    await socketClient.GetUser((ulong)rems.First().Author).SendMessageAsync(null, false, eb.Build(), null);
                }
            }
            catch (Exception)
            {
                await user.SendMessageAsync("Sorry, we didn't find any reminders for you", false, null, null);
            }
        }
    }
}