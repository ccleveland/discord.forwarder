using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using Discord;
using Discord.WebSocket;
using MailKit.Net.Imap;
using MailKit.Search;
using MimeKit;

namespace discord.forwarder.Services
{
    class Email
    {
        private string username;
        private string password;
        private string imapurl;

        private const int MAX_MESSAGE_LENGTH = 1975;
        public Email(string _imapurl, string _username, string _password)
        {
            imapurl = _imapurl;
            username = _username;
            password = _password;
        }

        public async void NotifyEmail(DiscordSocketClient client, UInt64 guildId, UInt64 channel, bool filter, string[] fromFilter)
        {
            while (true)
            {
                ArrayList messages = null;
                messages = await EmailQueryAsync(fromFilter, filter);
                foreach (MimeMessage m in messages)
                {
                    await ForwardMesage(client, guildId, channel, m.Subject, m.TextBody);
                }
                await Task.Delay(TimeSpan.FromMinutes(5));
            }
        }
        public async Task<ArrayList> EmailQueryAsync(string[] query, bool filter)
        {
            {
                var result = new ArrayList();
                using (var client = new ImapClient())
                {
                    await client.ConnectAsync(this.imapurl, 993, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync(this.username, this.password);
                    var folder = client.Inbox;
                    await folder.OpenAsync(MailKit.FolderAccess.ReadWrite);
                    List<MailKit.UniqueId> uids = new List<MailKit.UniqueId>();
                    if (filter)
                    {
                        foreach (string s in query)
                        {
                            uids = uids.Concat(client.Inbox.Search(SearchQuery.NotSeen.And(SearchQuery.FromContains(s)))).ToList();
                        }
                        foreach (var uid in uids)
                        {
                            var message = await folder.GetMessageAsync(uid);
                            MimeMessage email = new MimeMessage(message.From, message.To, message.Subject, message.Body);
                            if (message.Date > Process.GetCurrentProcess().StartTime)
                            {
                                result.Add(email);
                                await folder.AddFlagsAsync(uid, MailKit.MessageFlags.Seen, true);
                            }
                        }
                    }
                    else
                    {
                        foreach (var uid in client.Inbox.Search(SearchQuery.NotSeen))
                        {
                            var message = await folder.GetMessageAsync(uid);
                            MimeMessage email = new MimeMessage(message.From, message.To, message.Subject, message.Body);
                            if (message.Date >= DateTime.Today)
                            {
                                result.Add(email);
                                await folder.AddFlagsAsync(uid, MailKit.MessageFlags.Seen, true);
                            }
                        }
                    }
                    await client.DisconnectAsync(true);
                }
                return result;
            }
        }
        public async Task ForwardMesage(DiscordSocketClient client, UInt64 guildName, UInt64 channelName, string subject, string message)
        {
            var server = (SocketGuild)client.GetGuild(guildName);
            var channel = (SocketTextChannel)server.GetTextChannel(channelName);

            if (channel != null)
            {
                if (message.Length < MAX_MESSAGE_LENGTH)
                {
                    EmbedBuilder e = new EmbedBuilder();
                    e.AddField(subject, message);
                    await channel.SendMessageAsync(null, false, e.Build(), null);
                }
                else
                {
                    var sects = ChunksUpTo(message, MAX_MESSAGE_LENGTH);
                    int i = 1;
                    foreach (string s in sects)
                    {
                        EmbedBuilder e = new EmbedBuilder();
                        e.AddField(subject + " Part: " + i, s);
                        await channel.SendMessageAsync(null, false, e.Build(), null);
                        i++;
                        await Task.Delay(TimeSpan.FromSeconds(2));
                    }
                }
            }
        }
        private static IEnumerable<string> ChunksUpTo(string str, int chunkSize)
        {
            for (int i = 0; i < str.Length; i += chunkSize)
                yield return str.Substring(i, Math.Min(chunkSize, str.Length - i));
        }
    }
}