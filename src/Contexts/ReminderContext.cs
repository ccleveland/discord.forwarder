using Microsoft.EntityFrameworkCore;
using discord.forwarder.Models;
using Microsoft.Extensions.Logging;
using EFLogging;

namespace discord.forwarder
{
    public class ReminderContext : DbContext
    {
        private string ConnStr;
        private bool Debug;
        public DbSet<Reminder> Reminders { get; set; }

        public ReminderContext(string connStr, bool debug)
        {
            ConnStr = connStr;
            Debug = debug;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(ConnStr);
            if(Debug)
            {
                var lf = new LoggerFactory();
                lf.AddProvider(new MyLoggerProvider());
                optionsBuilder.UseLoggerFactory(lf);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Reminder>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.ToTable("reminder");

                entity.HasIndex(e => e.Scheduled)
                    .HasName("scheduled_time");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Author).HasColumnName("author");

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.Message)
                    .HasColumnName("message")
                    .HasMaxLength(2000);

                entity.Property(e => e.Scheduled).HasColumnName("scheduled");
            });
        }
    }
}