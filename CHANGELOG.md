# Changelog
## [Release 1.0.0](https://git.clevelandcoding.com/ccleveland/discord.forwarder/tags/v1.0.0)(11-5-18)
### Enhancements:
#### Discord Integration
* [x]  Login to Discord API
* [x]  Reply to Commands
* [x]  Send messages to specific channel
* [x]  Bot options to configuration file
  * [x] Make command prefix configurable
  * [x] Make default channel configurable
  * [x] Make "Admin" role configurable

#### Email Integration
Using dotnet mailkit package:
* [x] Login to imap email server  
* [x] Read emails from inbox/folder  
* [x] Read emails containing specifc subject/body text  
* [x] Outsource configuration to appsettings.json   
* [x] Make text to search subject and body for configurable  

### Fixed Bugs:
None

### Known Issues
None