# Discord.Forwarder

A dotnet core based discord bot to forward messages received from email and other sources to Discord channels.

## Getting Started

## Prerequisites
* Dotnet core 2.1.4 or greater

## Installing

### Install with Docker(recommended)

* docker run -d ccleveland/discord.forwarder -v path/to/dir:/etc/discord.forwarder/config
* See [docker-compose.yml](https://git.clevelandcoding.com/ccleveland/discord.forwarder/blob/dev/samples/docker-compose.yml) for a compose example
* Copy and update appsettings.json with your values in your /path/to/dir
### Install from source

1. Clone this repository
2. Run dotnet publish -f netcoreapp2.1 -c Release
3. Deploy the generated folder to your desired dotnet core compatible host
4. Copy and replace defaults with your values in appsettings.json 
5. Place appsettings.json in a directory relative to the root of discord.forwarder.dll ./configs/appsettings.json
6. Run with dotnet ./discord.forwarder.dll
7. TODO add running as a Windows and Linux Service

## Testing

Testing is done with the xunit testing kit.

## Contributing
Please email support@clevelandcoding.com or [open a ticket](https://help.clevelandcoding.com) if you are interested in contributing or have any issues, requests, or suggestions that you would like to submit

## Versioning

This repository uses [SemVer](https://semver.org/) for versioning. For the list of available and upcoming versions see the repository Releases tab.

## Authors

* Connor Cleveland - Maintainer

## License
This project is licensed under the MIT License - see the [license.md](https://git.clevelandcoding.com/ccleveland/discord.forwarder/blob/master/LICENSE) for details

## Thanks
* [Marlon Steve](https://www.iconfinder.com/marloncreations) for his outstanding take on the discord logo