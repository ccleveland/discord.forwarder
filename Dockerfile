FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

COPY ./src/*.csproj ./
RUN dotnet restore
COPY ./src ./
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/runtime:3.1.0-alpine3.10
COPY --from=build-env /app/out /etc/discord.forwarder
RUN mkdir /etc/discord.forwarder/config
WORKDIR /etc/discord.forwarder
ENTRYPOINT [ "dotnet", "discord.forwarder.dll" ]